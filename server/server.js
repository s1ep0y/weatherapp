const express = require('express');
const pug = require('pug');
const bodyParser = require('body-parser');
const weatherapi = require ('./weatherapi/weatherapi.js')

var app = express();
app.set('view engine', 'pug')
app.use(express.static('static'));
app.use(bodyParser.urlencoded({ extended: true }));

const port =  process.env.PORT || 3001;

app.get('/', (req, res) =>{
    res.render('index', {weather: 'Enter your place buddy'});
})

app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: 'You must provide an address!'
        })
    }

    weatherapi.geocode(req.query.address, (error, {lt, lg, place}) => {
        if (error) {
            return res.send({error})
            // return console.log(error)
        }
        weatherapi.forecast(lt, lg, ({sum, temp, precip, windSpeed}, error) => {
            if (error) {
                return res.render('index', {weather: error})
            }   
                var weatherout = `Today you should wait ${sum} Temperature will be ${temp}º celsius, chanse of precip ${precip} %. Wind speed about ${windSpeed} m/s`
                res.send({
                    weatherout,
                    place
                })
            })

    })
})

// //Вывод на коснольку во имя свтого
app.listen(port, () => {
    console.log(`Server is up on port ${port}`);
});