const request = require('request');

const forecast = (lt, lg, callback) => {
    request({
        url : `https://api.darksky.net/forecast/af1a15a527b00a1d367d16bef61188fc/${lt},${lg}?units=si&lang=en`,
        json: true
    }, (error, {body}) => {
        if (error) {
            callback(undefined, 'Unable to connect to weather service')
        } else if (body.error) {
            callback(undefined, 'Unable to find location')
        } else {
            var sum = body.daily.data[0].summary
            var temp = body.currently.temperature
            var precip = body.currently.precipProbability
            var windSpeed = body.currently.windSpeed
            callback({sum, temp, precip, windSpeed}, undefined)
        }
    })
}

const geocode = (place, callback) => {
    var place = encodeURIComponent(place)
    request(
        {
            url: `https://api.mapbox.com/geocoding/v5/mapbox.places/${place}.json?access_token=pk.eyJ1IjoiZGFubW9sIiwiYSI6ImNqc3d1NGh4djAxMnUzeXFvaDAwYTh0NHUifQ.dm9YcQjMht-O3GDk3RWPSQ&limit=1`,
            json: true
        },
        (error, {body}) => {
            if (error) {
                callback('Unable to connect to geolocation services', {})
            } else if (body.features.length == 0) {
                callback('Unable to find location', {})
            } else {
                var lg = body.features[0].center[0]
                var lt = body.features[0].center[1]
                var place =body.features[0].place_name
                callback(undefined, {lg, lt, place})
                // console.log(`lg: ${lg}, lt: ${lt}`) console.log(place) console.log(lg)
            }
        }
    )
}

module.exports = {
    geocode,
    forecast
};